extends KinematicBody2D

var MOTION = Vector2()

func _physics_process(delta):
	if Input.is_action_just_pressed("ui_right"):
		MOTION.x = 900
	elif Input.is_action_just_pressed("ui_left"):
		MOTION.x = -900		
	else:
		MOTION.x = 0
		
	move_and_slide(MOTION)